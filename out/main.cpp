#include <stdlib.h>
#include <modbus/modbus.h>
#include <iostream>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <ctime>
#include <stdio.h>
#include <thread>
#include <cstdlib>

using namespace std;

int main(int argc ,char *argv[])
{

  modbus_t *mb;
  uint16_t tab_reg[32];
  for (int i=0; i < 32 ; i++)
  tab_reg[i] = 0 ;

  char * pEnd;
  
  int baud = strtol(argv[2], &pEnd, 10);
  int data_bit = strtol(argv[4], &pEnd, 10);
  int stop_bit = strtol(argv[5], &pEnd, 10);
  int startReg = strtol(argv[6], &pEnd, 10);
  int regCount = strtol(argv[7], &pEnd, 10);
  int rtsState = strtol(argv[8], &pEnd, 10);
  int slaveNum = strtol(argv[9], &pEnd, 10);
  int timeout_sec = strtol(argv[10], &pEnd, 10);
  int timeout_usec = strtol(argv[11], &pEnd, 10);
  int request_delay = strtol(argv[12], &pEnd, 10); 
  char parity = argv[3][0];

/*
  mb = modbus_new_rtu(argv[1], int baud, char parity,
                                    int data_bit, int stop_bit);
  */
  mb = modbus_new_rtu( argv[1] ,baud ,parity ,data_bit ,stop_bit );
  modbus_set_slave(mb,slaveNum);
  modbus_rtu_set_serial_mode(mb,MODBUS_RTU_RS485);
  modbus_rtu_set_rts(mb,rtsState);
  modbus_set_response_timeout(mb,timeout_sec,timeout_usec);
  modbus_connect(mb);

  /* Read 5 registers from the address 0 */
 for(;;)
 {
  int start_s = clock();
  int res = modbus_read_registers(mb, startReg, regCount, tab_reg);
  int stop_s = clock();

  cout << (stop_s - start_s)/double(CLOCKS_PER_SEC)*1000 << endl;

  if(res == -1)
    cout << "error reply for request ..." << endl; 

 for (int i=0; i < res ; i++)
  cout << tab_reg[i] << endl;
  usleep(request_delay);
 }
  modbus_close(mb);
  modbus_free(mb);

return 0;
}
